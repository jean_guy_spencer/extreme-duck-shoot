using UnityEngine;
using System.Collections;

public static class Scoring
{
    public static void setScore(string name, int _s) { PlayerPrefs.SetInt(name, _s); }
    public static int getScore(string name) { return PlayerPrefs.GetInt(name); }

    public static void addToScore(string name, int _s) { PlayerPrefs.SetInt(name, PlayerPrefs.GetInt(name) + _s); }

    public static void ResetScore(string p1, string p2) 
    { 
        //PlayerPrefs.DeleteAll();
        setScore(p1 + "YD", 0);
        setScore(p1 + "GD", 0);
        setScore(p1 + "PH", 0);
        setScore(p2 + "YD", 0);
        setScore(p2 + "GD", 0);
        setScore(p2 + "PH", 0);
    }

    public static void YellowDuckHit(string playerName)
    {
        addToScore(playerName + "YD", 1);
    }
    
    public static void GreenDuckHit(string playerName)
    {
        addToScore(playerName + "GD", 1);
    }

    public static void PlayerHit(string playerName)
    {
        addToScore(playerName + "PH", 1);
    }

    public static int GetYellowDuckHits(string playername)
    {
        return PlayerPrefs.GetInt(playername + "YD");
    }

    public static int GetGreenDuckHits(string playername)
    {
        return PlayerPrefs.GetInt(playername + "GD");
    }

    public static int GetPlayerHits(string playername)
    {
        return PlayerPrefs.GetInt(playername + "PH");
    }
}
