﻿using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour
{
		public Transform zoomPos;
		public Transform coverPos;
		public Sprite hitSprite;
		float speed = 75.0f;
		public ReticleController reticle;
		float yAxis;
		float xAxis;
		public bool camIsZoomedIn { get; private set; }
		
		public Player player;
		Vector3 scale;
		//public float blindnessTime;
		//float timer;

		// Use this for initialization
		void Start ()
		{
				
				scale = new Vector3 (7, 7, 7);
				GetComponent<SpriteRenderer> ().transform.localScale = scale;
				//timer = 0;
				camIsZoomedIn = false;
		}

		public void Hit ()
		{
				//Debug.Log ("HIT " + player.name);
				//player.otherPlayer.cam.gameObject.GetComponent<SpriteRenderer> ().enabled = true;

		}

		public void SetAxies(float x, float y) {
			yAxis = y;
			xAxis = x;
		}

		public void SetZoom(bool isZoomed)
		{
			camIsZoomedIn = isZoomed;
		}

		public void ToggleZoom ()
		{
				if (!camIsZoomedIn) {
							
						this.camIsZoomedIn = true;
				} else {
						
						this.camIsZoomedIn = false;
				}
		}

		// Update is called once per frame
		void Update ()
		{
				if (GetComponent<SpriteRenderer> ().enabled && !player.recovering) {
						//GetComponent<SpriteRenderer> ().enabled = false;
				}

				// zoom out if cover input is detected
				if (yAxis < -0.5f || xAxis > 0.5f || xAxis < -0.5f || player.recovering) {
				//if (player.recovering) {
						if (transform.position.z == coverPos.position.z && camIsZoomedIn) {
								//Debug.Log("Cam is Zoomed OUT");
								player.Cover();
				reticle.Toggle();
								camIsZoomedIn = false;
						} else if (transform.position.z != coverPos.position.z) {
								//Debug.Log("Zooming out...");
								float step = speed * Time.deltaTime;
								transform.position = Vector3.MoveTowards (transform.position, coverPos.position, step);
						}
				}

        // zoom in if cover input is released
        else {
			//Debug.Log(yAxis);
						if (transform.position.z == zoomPos.position.z && !camIsZoomedIn) {
								//Debug.Log("Cam is Zoomed IN");
								player.Aim ();
				reticle.Toggle();
								camIsZoomedIn = true;
						}

						if (transform.position.z != zoomPos.position.z) {
								//Debug.Log("Zooming in...");
								
								float step = speed * Time.deltaTime;
								transform.position = Vector3.MoveTowards (transform.position, zoomPos.position, step);
						}
				}
		}
}
