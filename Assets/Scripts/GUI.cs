﻿using UnityEngine;
using System.Collections;

public class GUI : MonoBehaviour {

	public void ChangeScene(string sceneName) {
		Application.LoadLevel (sceneName);
	}

	public void ExitGame() {
		Application.Quit ();
	}
}