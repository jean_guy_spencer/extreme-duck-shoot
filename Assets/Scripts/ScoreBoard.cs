﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreBoard : MonoBehaviour {
    public Text P1Score;
    public Text P2Score;
    public Text winner;

    string P1name, P2name;

    int P1Y, P1G, P1H;
    int P1YT, P1GT, P1HT, P1Total;

    int P2Y, P2G, P2H;
    int P2YT, P2GT, P2HT, P2Total;
    


    // Use this for initialization
	void Start ()
    {
        P1name = "Player1";
        P2name = "Player2";

        //DebugScores();

        GetAllScores();
        GetWinner();

        P1Score.text = "YELLOW DUCKS x " + P1Y + " = " + P1YT+
            "\nGREEN DUCKS x " + P1G + " = " + P1GT+
            "\nSHOT BY P2 x " + P1H + " = " + P1HT+
            "\nTOTAL = " + P1Total;

        P2Score.text = "YELLOW DUCKS x " + P2Y + " = " + P2YT +
            "\nGREEN DUCKS x " + P2G + " = " + P2GT +
            "\nSHOT BY P1 x " + P2H + " = " + P2HT +
            "\nTOTAL = " + P2Total;
    }

    void DebugScores()
    {
        Scoring.YellowDuckHit(P1name);
        Scoring.YellowDuckHit(P1name);
        Scoring.YellowDuckHit(P1name);
        Scoring.YellowDuckHit(P2name);
        Scoring.YellowDuckHit(P2name);
    }

    void GetWinner()
    {
        if (P1Total > P2Total)
            winner.text = "WINNER IS\nPLAYER 1!";

        else if (P1Total == P2Total)
            winner.text = "TIE GAME!";

        else
            winner.text = "WINNER IS\nPLAYER 2!";
    }

    void GetAllScores()
    {
        P1Y = Scoring.GetYellowDuckHits(P1name);
        P1YT = P1Y * 10;

        P1G = Scoring.GetGreenDuckHits(P1name);
        P1GT = P1G * 50;

        P1H = Scoring.GetPlayerHits(P1name);
        P1HT = P1H * -20;

        P2Y = Scoring.GetYellowDuckHits(P2name);
        P2YT = P2Y * 10;

        P2G = Scoring.GetGreenDuckHits(P2name);
        P2GT = P2G * 50;

        P2H = Scoring.GetPlayerHits(P2name);
        P2HT = P2H * -20;

        P1Total = P1YT + P1GT + P1HT;
        P2Total = P2YT + P2GT + P2HT;
    }

	// Update is called once per frame
	void Update () {
	
	}
}
