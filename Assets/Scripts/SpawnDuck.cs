﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnDuck : MonoBehaviour {
    public GameObject duckPrefab;
    public float spawnInterval;
    public Vector3 scale;
    public float duckSpeed;
    public Color duckColor;
    public int points;

    float lastInterval;
    
	// Use this for initialization
	void Start () {
        lastInterval = 0;
	}
	
	// Update is called once per frame
	void Update () {
        lastInterval += Time.deltaTime;

        if (lastInterval >= spawnInterval)
        {
			//Vector3 v = new Vector3(transform.position.x, transform.position.y, 0);
            GameObject temp = Instantiate(duckPrefab, transform.position, transform.rotation) as GameObject;
			temp.transform.localScale = scale;
            temp.renderer.material.color = duckColor;
            DuckMovement duck = temp.GetComponent<DuckMovement>();
            duck.SetSpeed(duckSpeed);
            duck.SetPoints(points);

            lastInterval = 0;
        }
	}
}
