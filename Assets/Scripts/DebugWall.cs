﻿using UnityEngine;
using System.Collections;

public class DebugWall : MonoBehaviour
{
    public Transform P1;
    public Transform P2;
    bool wallIsUp;
    public Animation wallAnim;
    public AnimationClip wallGoesDown;
    public AnimationClip wallGoesUp;
    int numberOfHits;
    public int hitsToFall;
    float timer;
    float randomTimeToRise;
    public AudioClip musicWallDown;
    public AudioClip musicWallUp;

    public float gameTime;
    float gameTimer;

    // Use this for initialization
    void Start()
    {
        Scoring.ResetScore(P1.name, P2.name);
        gameTimer = 0;
        audio.clip = musicWallUp;
        audio.Play();
        timer = 0;
        numberOfHits = 0;
        //wallAnim = transform.parent.GetComponent<Animation>();
		Debug.Log (wallAnim);
        wallIsUp = true;
        randomTimeToRise = 10.0f;
    }

    public void Hit()
    {
        numberOfHits++;

        if (numberOfHits >= hitsToFall)
        {
            WallDown();
        }
    }

    void WallDown()
    {
        collider.enabled = false;
        wallAnim.clip = wallGoesDown;
        wallAnim.Play();
        wallIsUp = false;
        randomTimeToRise = Random.Range(10.0f, 20.0f);
        audio.Stop();
        audio.clip = musicWallDown;
        audio.Play();
    }

    void WallUp()
    {
        collider.enabled = true;
        wallAnim.clip = wallGoesUp;
        wallAnim.Play();
        wallIsUp = true;
        timer = 0;
        audio.Stop();
        audio.clip = musicWallUp;
        audio.Play();
		numberOfHits = 0;
    }

    // Update is called once per frame
    void Update()
    {
        gameTimer += Time.deltaTime;

        if (gameTimer >= gameTime)
            Application.LoadLevel("finalscore");

        if (!wallIsUp)
        {
            timer += Time.deltaTime;
            //Debug.Log(timer);
            if (timer >= randomTimeToRise)
            {
                //Debug.Log("TIMER STOP");
                WallUp();
            }
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            WallUp();
        }
    }
}
