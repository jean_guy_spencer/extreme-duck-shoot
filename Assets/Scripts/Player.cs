﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    public Transform aimPos;
    public Transform coverPos;
    public Sprite aimSprite;
    public Sprite coverSprite;
    public PlayerCamera cam;
    public Player otherPlayer;
    public float recoverTime;
    float timer;
    public bool recovering { get; private set; }


    // Use this for initialization
    void Start()
    {
        recovering = false;
        timer = 0;
    }

    public void Hit()
    {
		//Debug.Log (name + " HIT!");
        ForceCover();
    }

    public void ForceCover()
    {
		Scoring.PlayerHit (name);
		Debug.Log (name + " got hit");
		//Debug.Log ("Force Cover");
		cam.Hit ();
		Cover ();
        recovering = true;
    }

	public void Cover()
	{
		collider.enabled = false;
		GetComponent<SpriteRenderer>().sprite = coverSprite;
		transform.position = coverPos.position;
	}

	public void Aim()
	{
		collider.enabled = true;
		GetComponent<SpriteRenderer>().sprite = aimSprite;
		transform.position = aimPos.position;
	}

    void Recover()
    {
		Aim ();
        recovering = false;
		timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (recovering)
        {
            timer += Time.deltaTime;

            if (timer >= recoverTime)
            {
                Recover();
            }
        }
    }
}
