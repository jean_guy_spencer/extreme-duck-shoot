﻿using UnityEngine;
using System.Collections;

public class DuckMovement : MonoBehaviour {
    public Sprite liveDuck;
    public Sprite deadDuck;
    public float speed;// { get; private set; }
    public float points;// { get; private set; }
    Vector3 startPos;
    public float travelDistance;
    bool dead;
    public float secondsToDie;
    float timeOfDeath;
    float deathTimer;
	public GameObject feathers;

	// Use this for initialization
	void Start () {
        deathTimer = 0;
        dead = false;
        startPos = transform.localPosition;
	}

    public void Hit()
    {
        GetComponent<SpriteRenderer>().sprite = deadDuck;
        dead = true;
        collider.enabled = false;
        timeOfDeath = Time.time;
		Instantiate (feathers, transform.position, Quaternion.identity);
    }

    public void SetSpeed(float val)
    {
        speed = val;
    }

    public void SetPoints(int p)
    {
        points = p;
    }

	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKeyDown(KeyCode.Z))
            Hit();

        if (!dead)
        {
            float step = speed * Time.deltaTime;
            transform.position -= transform.right * step;

            if (Vector3.Distance(transform.position, startPos) >= Mathf.Abs(travelDistance))
                Destroy(gameObject);
        }

        else
        {
            if (renderer.enabled)
                renderer.enabled = false;
            else
                renderer.enabled = true;

            deathTimer += Time.deltaTime;

            if (deathTimer >= secondsToDie)
                Destroy(gameObject);
        }
	}
}
