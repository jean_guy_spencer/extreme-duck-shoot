﻿using UnityEngine;
using System.Collections;

public class ReticleController : MonoBehaviour
{
		float inputRX;
		float inputRY;
		float inputLX;
		float inputLY;
		float inputA;
		float inputB;
		float objX;
		float objY;
		float dampener = 0.1f;
		public GameObject particle_emitter;
		Vector3 ray;
		public PlayerCamera cam;
		public Player player;
		public string playerName;
		float xPos = 0.0f;
		float neg = 1.0f;
		string RxAxis;// = playerName + "_LA_X";
		string RyAxis;// = playerName + "_LA_Y";
		string LyAxis;
		string LxAxis;
		string shoot;// = playerName + "_A";
		bool toggleZoom;
		public float recoilTime;
		float recoilTimer;
		bool recoil;

		public bool reticleActive { get; private set; }

		public void Toggle ()
		{
				if (reticleActive) {
						GetComponent<SpriteRenderer> ().enabled = false;
						reticleActive = false;	
				} else {
						GetComponent<SpriteRenderer> ().enabled = true;
						reticleActive = true;		
				}
		}
		// Use this for initialization
		void Start ()
		{
				reticleActive = false;
				RxAxis = "R_XAxis_" + playerName;
				RyAxis = "R_YAxis_" + playerName;
				LxAxis = "L_XAxis_" + playerName;
				LyAxis = "L_YAxis_" + playerName;
				//shoot = "A_" + playerName;
				shoot = "RB_" + playerName;
				toggleZoom = true;
				recoil = false;
				recoilTimer = recoilTime;
				//Debug.Log (LxAxis);
				//Debug.Log (LyAxis);
				Debug.Log (shoot);
		}
	
		// Update is called once per frame
		void Update ()
		{
				inputLX = Input.GetAxis (LxAxis);
				inputLY = Input.GetAxis (LyAxis);
				if (inputLX != 0) {
						//Debug.Log ("Player " + playerName + "inputLX = " + inputLX);
				}
				if (inputLY != 0) {
						//Debug.Log ("Player " + playerName + "inputLY = " + inputLY);
				}
				//if ( (inputLX > 0.3f || inputLX < -0.3f) && !toggleZoom) {
		
		
				cam.SetAxies (inputLX, inputLY);
				if (!reticleActive) {
						//Debug.Log("Reticle not active");	
						return;
				}
				Vector3 playerSize = renderer.bounds.size;
				var distance = (this.gameObject.transform.position - cam.camera.transform.position).z;
	
				var leftBorder = cam.camera.ViewportToWorldPoint (new Vector3 (0, 0, distance)).x + (playerSize.x / 2);
				var rightBorder = cam.camera.ViewportToWorldPoint (new Vector3 (1, 0, distance)).x - (playerSize.x / 2);

				if (playerName == "1") {
						xPos = 0.5f;
						neg *= -1.0f;
				}
				var bottomBorder = cam.camera.ViewportToWorldPoint (new Vector3 (xPos, 0, distance)).y + (neg * playerSize.y / 2);
				var topBorder = cam.camera.ViewportToWorldPoint (new Vector3 (xPos, 1, distance)).y - (neg * playerSize.y / 2);

				inputRX = Input.GetAxis (RxAxis);
				if (inputRX != 0) {
						//Debug.Log("Player " + playerName + " Input X = " + inputRX.ToString());
						//Debug.Log("this.gameobject.name.inputx = " + this.gameObject.name);
						objX = this.gameObject.transform.position.x;
						objX += (inputRX * dampener);
						//gameObject.transform.position.Set(objX, gameObject.transform.position.y, gameObject.transform.position.z);
						this.gameObject.transform.position = new Vector3 (Mathf.Clamp (objX, leftBorder, rightBorder), 
			                                 this.gameObject.transform.position.y, 
			                                this.gameObject.transform.position.z);
				}

				inputRY = Input.GetAxis (RyAxis);
				if (inputRY != 0) {
						//Debug.Log ("Player " + playerName + "Input Y = " + inputRY.ToString ());
						objY = this.gameObject.transform.position.y;
						objY += (inputRY * dampener);
						if (playerName == "2") {
								this.gameObject.transform.position = new Vector3 (this.gameObject.transform.position.x, 
			                                 Mathf.Clamp (objY, bottomBorder, topBorder), 
			                                 this.gameObject.transform.position.z);
						} else if (playerName == "1") {
								this.gameObject.transform.position = new Vector3 (this.gameObject.transform.position.x, 
				                                                 Mathf.Clamp (objY, topBorder, bottomBorder), 
				                                                 this.gameObject.transform.position.z);
						}
				}
				
				
//				if ( (inputLX == 1.0f || inputLX == -1.0f)) 
//				{
//					Debug.Log("Player " + playerName + "LX is not 0 = " + inputLX);
//					if (toggleZoom)
//					{
//						toggleZoom = false;
//						cam.SetZoom(false);
//					}
//				}
//				else if (inputLY == 1.0f)
//				{
//					Debug.Log("Player " + playerName + "LY is not 0 = " + inputLY);
//					if (toggleZoom)
//					{
//						toggleZoom = false;
//						cam.SetZoom(false);
//					}
//				}
//				else {
//						Debug.Log ("Player " + playerName + "toggleZoom is true, reset to false");
//						toggleZoom = true;
//						cam.SetZoom (true);
//				} 
				
//				if (inputLX >= 0.4f || inputLX <= -0.4f) {
//						//Debug.Log("left X");
//						Debug.Log ("inputLX = " + inputLX);
//						if (!toggleZoom) {
//				Debug.Log ("toggleZoom");
//								toggleZoom = true;
//								cam.ToggleZoom ();
//						}
//				}
//				else if (inputLY < -0.3f && !toggleZoom) {
//						//Debug.Log ("left Y");
//						toggleZoom = true;
//						cam.ToggleZoom ();
//
//				} else {
//						cam.ToggleZoom ();
//						toggleZoom = false;
//
//				}

				//inputA = 0;
				this.inputA = Input.GetAxis (shoot);
				Debug.Log(player.name + "shoot axis = " + inputA);
				//inputA = Input.GetButtonDown (shoot);
				//Debug.Log ("shoot buttondown = " + Input.GetButtonDown (shoot));
				//if (inputA > 0.3f) {
				if (this.recoil) {
						this.recoilTimer -= Time.deltaTime;
				}

				if (this.recoilTimer <= 0) {
						this.recoil = false;
						this.recoilTimer = this.recoilTime;
				}

				//if (this.inputA > 0.9f && !recoil) {
				if (Input.GetButtonDown(shoot) && !recoil) {
						Debug.Log (player.name + " inputA > 0.3f");
						this.recoil = true;
						ray = this.gameObject.transform.TransformDirection (Vector3.forward) * 100;
						Debug.DrawRay (this.gameObject.transform.position, ray, Color.green);
						Vector3 screenPos = cam.camera.WorldToScreenPoint (this.transform.position);
						Ray ray2 = cam.camera.ScreenPointToRay (screenPos);
						//Debug.DrawRay (ray2.origin, ray2.direction, Color.green);			
						RaycastHit hit;
						//RaycastHit hit = Physics.Raycast (this.gameObject.transform.position, this.gameObject.transform.TransformDirection (Vector3.forward)*100);
						bool yay = Physics.Raycast (ray2.origin, ray2.direction, out hit);
						//RaycastHit2D hit = Physics2D.Raycast(camera2D, mousePosition, distance (optional));
						if (yay) {// != null && hit.collider != null) {
								//Debug.Log ("hit.collider.name = " + hit.collider.name);
								if (hit.collider.tag == "Player") {
										hit.collider.gameObject.GetComponent<Player> ().Hit ();
								} else if (hit.collider.name == "Duck(Clone)") {
										//Debug.Log("HIT!");
										if (hit.collider.gameObject.renderer.material.color == Color.white) {
												Scoring.YellowDuckHit (player.name);
												Debug.Log (player.name + " hit yellow ducks");
										} else {
												Scoring.GreenDuckHit (player.name);
												Debug.Log (player.name + " hit green ducks");
										}
										Instantiate (particle_emitter, hit.transform.position, Quaternion.identity);
										hit.collider.gameObject.GetComponent<DuckMovement> ().Hit ();
								} else if (hit.collider.name == "Wall Sprite") {
										Debug.Log (player.name + "HIT!");
										Instantiate (particle_emitter, hit.point, Quaternion.identity);
										hit.collider.gameObject.GetComponent<DebugWall> ().Hit ();
								}
								
						}
				}
		}
}
